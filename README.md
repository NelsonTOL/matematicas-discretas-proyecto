# Key-Security :desktop_computer:
## Presentación del proyecto :rocket:

------------

> Es bien sabido, que la seguridad informática se enfoca en la protección de la infraestructura computacional, y específicamente aquella información sensible circulante en las de redes computacionales. Es por esto, que es fundamental prevenir ataques maliciosos que puedan hacer uso indebido de dicha información. Actualmente se presentan cada vez mas violaciones a la seguridad informática que alcanza hasta las criptomonedas y seguridad nacional de los países.

De allí, la idea principal de proteger se basa en la encriptación de notas o cadenas de texto, que las personas proporcionan pero necesitan ser protegidas.

Dicha protección, se puede lograr implementando varios niveles de encriptación, con la creación de llaves automáticas para dessencriptar dichos datos, utilizando para esto Python.

En este sentido, el proyecto surge de la necesidad de solventar un gran problema como el robo de datos de vital importancia, que han venido preparandose desde los últimos años.

Para esto, ha de buscarse una forma efectiva de minimizar los ataques ciberneticos futuros y fortalecer la postura de seguridad de los usuarios.

> <div style="text-align:center"><img src="https://drive.google.com/file/d/1LkNm2UF-YndBaE8q1NtVTasp48L_vU85/view?usp=sharing" /></div>



### Problematica a abordar :gear:
- Esta idea surge por la problematica del robo de datos importantes de usuarios que se ha venido manifestandose desde tiempo atrás. Queremos buscar alguna forma de no dejar a simple vista la informacion valiosa de los usuarios.
- Esto puede ser utilizado practicamente en cualquier contexto donde tengamos una base de datos que los usuarios van llenando progresivamente, en un banco, una pagina web que almacene cuentas de usuarios, etc.
